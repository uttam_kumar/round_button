//
//  ViewController.swift
//  RoundButton
//
//  Created by Syncrhonous on 17/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var roundBtn: UIButton!
    
    @IBOutlet weak var lToast: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.applyRoundCorner(roundBtn)
        self.applyRoundCorner(lToast)
    }
    
    func applyRoundCorner(_ object:AnyObject){
        object.layer.cornerRadius = object.frame.size.width / 2
        object.layer?.masksToBounds = true
        
    }
    
    
    
    @IBAction func shortToastDisplay(_ sender: UIButton) {
        ToastView.shared.short(self.view, txt_msg: "Short Message")
    }
    
    @IBAction func longToastDisplay(_ sender: Any) {
        ToastView.shared.long(self.view, txt_msg: "Long Message")
    }
    
}

